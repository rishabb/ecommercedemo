//
//  AppUtils.h
//  eCommerceDemo
//
//  Created by Rishab Bokaria on 03/04/16.
//  Copyright © 2016 Rishab Bokaria. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppUtils : NSObject

+ (NSString *)clearNull:(NSString *)aString;
+ (NSString *)getKeyFromURL:(NSString *)aURLString;
+ (NSString *)getAssetKeyFromURL:(NSString *)aURLString;
+ (NSString *)getCacheImagesDirectoryPath;

@end
