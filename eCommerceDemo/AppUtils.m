//
//  AppUtils.m
//  eCommerceDemo
//
//  Created by Rishab Bokaria on 03/04/16.
//  Copyright © 2016 Rishab Bokaria. All rights reserved.
//

#import "AppUtils.h"

@implementation AppUtils

+ (NSString *)clearNull:(NSString *)aString;
{
    if(aString == (id)[NSNull null])
    {
        aString = nil;
    }
    return aString;
}

+ (NSString *)getKeyFromURL:(NSString *)aURLString
{
    NSString *aURLkey = [aURLString stringByReplacingOccurrencesOfString:@"/" withString:@""];
    aURLkey = [aURLkey stringByReplacingOccurrencesOfString:@"www" withString:@""];
    aURLkey = [aURLkey stringByReplacingOccurrencesOfString:@"http:" withString:@""];
    aURLkey = [aURLkey stringByReplacingOccurrencesOfString:@"." withString:@""];
    aURLkey = [aURLkey stringByReplacingOccurrencesOfString:@" " withString:@""];
    return aURLkey;
}

+ (NSString *)getAssetKeyFromURL:(NSString *)aURLString
{
    NSString *pathExtension = [aURLString pathExtension];
    NSString *aURLKey = [AppUtils getKeyFromURL:aURLString];
    aURLKey = [aURLKey stringByAppendingPathExtension:pathExtension];
    return aURLKey;
}

+ (NSString *)getCacheImagesDirectoryPath
{
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains (NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cachesDirectory = dirPaths[0];
    NSString *cacheImageDirectory = [cachesDirectory stringByAppendingPathComponent:@"images"];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:cacheImageDirectory])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:cacheImageDirectory withIntermediateDirectories:YES attributes:nil error:nil];
    }
    return cacheImageDirectory;
}

@end
