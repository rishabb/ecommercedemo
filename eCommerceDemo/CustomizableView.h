//
//  CustomizableView.h
//  CustomizableView
//
//  Created by Rishab Bokaria on 20/03/15.
//  Copyright (c) 2015 Rishab Bokaria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomizableView : UIView

/********* Subclass this class to create any custom view with a interface builder binded to it. *********/
 
/******
 *
 *
  Provided, please make sure that these requirements are met:-
  1.The interface builder name should be same as your custom view class name.
  2.Your interface builder's file owner is set to your custom view class.
  3.If you override any of the UIView's methods then dont forget to call the same on super.
  4.Your interface builder should have only one independent view.
  5.Override  - (void)nibLoaded; method in your custom view.
 *
 *
******/

- (UIView *)nibView;
- (void)nibLoaded;

@end
