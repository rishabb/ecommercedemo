//
//  CustomizableView.m
//  CustomizableView
//
//  Created by Rishab Bokaria on 20/03/15.
//  Copyright (c) 2015 Rishab Bokaria. All rights reserved.
//

#import "CustomizableView.h"

@interface CustomizableView ()
{
    UIView *_nibView;
}
@end

@implementation CustomizableView

#pragma mark -
#pragma mark - Initalization Methods
#pragma mark -

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        [self loadNib];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self loadNib];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        [self loadNib];
    }
    return self;
}

#pragma mark -
#pragma mark - Layout Updation Methods
#pragma mark -

- (void)layoutSubviews
{
    [super layoutSubviews];
    _nibView.frame = self.bounds;
}

#pragma mark -
#pragma mark - Helper Methods
#pragma mark -

- (void)loadNib
{
    _nibView = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] objectAtIndex:0];
    _nibView.frame = self.bounds;
    [self addSubview:_nibView];
//    self.backgroundColor = _nibView.backgroundColor = [UIColor clearColor];
    [self nibLoaded];
}

- (UIView *)nibView
{
    return _nibView;
}

#pragma mark -
#pragma mark - View Customization Methods
#pragma mark -

- (void)nibLoaded
{
    //do nothing!
}

@end
