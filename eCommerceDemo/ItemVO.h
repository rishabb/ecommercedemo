//
//  ItemVO.h
//  eCommerceDemo
//
//  Created by Rishab Bokaria on 03/04/16.
//  Copyright © 2016 Rishab Bokaria. All rights reserved.
//
#import <Foundation/Foundation.h>

@interface ItemVO : NSObject

- (instancetype)initWithItemInfo:(NSDictionary *)itemInfo;
- (NSString *)label;
- (NSString *)imageURL;
- (NSString *)webURL;

@end
