//
//  ItemVO.m
//  eCommerceDemo
//
//  Created by Rishab Bokaria on 03/04/16.
//  Copyright © 2016 Rishab Bokaria. All rights reserved.
//

#import "ItemVO.h"
#import "AppUtils.h"

NSString* const ITEM_INFO_LABEL_KEY     =   @"label";
NSString* const ITEM_INFO_IMAGE_KEY1    =   @"image";
NSString* const ITEM_INFO_IMAGE_KEY2    =   @"image_url";
NSString* const ITEM_INFO_WEB_URL_KEY   =   @"web-url";

@interface ItemVO ()
{
    NSString *_labelString;
    NSString *_imageURL;
    NSString *_webURL;
}
@end

@implementation ItemVO

- (instancetype)initWithItemInfo:(NSDictionary *)itemInfo
{
    self = [super init];
    if (self)
    {
        _labelString = [AppUtils clearNull:[itemInfo valueForKey:ITEM_INFO_LABEL_KEY]];
        _imageURL = [AppUtils clearNull:[itemInfo valueForKey:ITEM_INFO_IMAGE_KEY1]];
        _webURL = [AppUtils clearNull:[itemInfo valueForKey:ITEM_INFO_WEB_URL_KEY]];
        if (!_imageURL)
        {
            _imageURL = [AppUtils clearNull:[itemInfo valueForKey:ITEM_INFO_IMAGE_KEY2]];
        }
    }
    return self;
}

- (NSString *)label
{
    return _labelString;
}

- (NSString *)imageURL
{
    return _imageURL;
}

- (NSString *)webURL
{
    return _webURL;
}

@end
