//
//  LandingViewController.m
//  eCommerceDemo
//
//  Created by Rishab Bokaria on 02/04/16.
//  Copyright © 2016 Rishab Bokaria. All rights reserved.
//

#import "LandingViewController.h"
#import "SideDrawerView.h"
#import "MainScreenView.h"
#import "WebViewVC.h"
#import "SearchVC.h"

float const SideDrawerAnimationSpeed      =       0.25;

@interface LandingViewController ()<MainScreenViewDelegate>
{
    __weak IBOutlet SideDrawerView          *_masterHolderView;
    __weak IBOutlet MainScreenView          *_detailHolderView;
    __weak IBOutlet UIButton                *_hideSideDrawerButton;
    __weak IBOutlet NSLayoutConstraint      *_sideDrawerleadingSpaceConstraint;
    
    CGRect          _frameAtPanBegan;
    BOOL            _panDirectionRight;
    NSString        *_webViewVCURLString;
}
- (IBAction)onClickHideButton:(id)sender;
@end

@implementation LandingViewController

#pragma mark - View Life Cycle & Styling

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    /** Add gestures to activate drag on side menu. **/
    [self addRequiredGestures];
    
    /** Add delegates to listen events from views. **/
    [self addEventDelegatesToSubViews];
    [_detailHolderView handleSideDrawerEvent:SideDrawerViewEventHome];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - Helper Methods

- (void)addRequiredGestures
{
    // Adding dragPanGesture
    UIPanGestureRecognizer *dragGesture = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(dragViewWithGesture:)];
    [dragGesture setMinimumNumberOfTouches:1];
    [dragGesture setMaximumNumberOfTouches:2];
    [_masterHolderView addGestureRecognizer:dragGesture];
    dragGesture = nil;
    
    UIScreenEdgePanGestureRecognizer *leftEdgeGesture = [[UIScreenEdgePanGestureRecognizer alloc] initWithTarget:self action:@selector(dragViewWithGesture:)];
    leftEdgeGesture.edges = UIRectEdgeLeft;
    [self.view addGestureRecognizer:leftEdgeGesture];
}

- (void)addEventDelegatesToSubViews
{
    [_masterHolderView setDelegate:_detailHolderView];
    [_detailHolderView setDelegate:self];
}

- (void)openSideDrawerWithAnimation:(BOOL)animate
{
    NSTimeInterval duration = (animate)?SideDrawerAnimationSpeed*1.5:SideDrawerAnimationSpeed;
    [self.view bringSubviewToFront:_masterHolderView];
    [UIView animateWithDuration:duration animations:^{
        _sideDrawerleadingSpaceConstraint.constant = _detailHolderView.frame.origin.x;
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self.view bringSubviewToFront:_hideSideDrawerButton];
        _hideSideDrawerButton.hidden = NO;
        [_detailHolderView setMenuSelected:YES];
    }];
}

- (void)closeSideDrawerWithAnimation:(BOOL)animate
{
    NSTimeInterval duration = (animate)?SideDrawerAnimationSpeed*1.5:SideDrawerAnimationSpeed;
    [UIView animateWithDuration:duration animations:^{
        _sideDrawerleadingSpaceConstraint.constant = _masterHolderView.frame.size.width * -1;
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        _hideSideDrawerButton.hidden = YES;
        [self.view sendSubviewToBack:_masterHolderView];
        [self.view sendSubviewToBack:_hideSideDrawerButton];
        [_detailHolderView setMenuSelected:NO];
    }];
}

- (void)showSearch
{
    SearchVC *searchVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([SearchVC class])];
    [self.navigationController pushViewController:searchVC animated:YES];
}

#pragma mark - Action Methods

- (IBAction)onClickHideButton:(id)sender
{
    [self closeSideDrawerWithAnimation:YES];
}

- (void)dragViewWithGesture:(UIPanGestureRecognizer*)gestureRecognizer
{
    CGPoint translatedPoint = [gestureRecognizer translationInView:gestureRecognizer.view];
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan)
    {
        CGPoint velocityOfPan = [gestureRecognizer velocityInView:gestureRecognizer.view];
        
        _frameAtPanBegan = _masterHolderView.frame;
        _panDirectionRight = (velocityOfPan.x > 0);
        _detailHolderView.userInteractionEnabled = NO;
        if (_panDirectionRight)
        {
            [self.view bringSubviewToFront:_masterHolderView];
        }
    }
    else if (gestureRecognizer.state == UIGestureRecognizerStateChanged)
    {
        CGRect translatedFrame = _frameAtPanBegan;
        translatedFrame.origin.x += translatedPoint.x;
        
        if ((CGRectGetMaxX(translatedFrame) <= CGRectGetWidth(translatedFrame)) && (CGRectGetMinX(translatedFrame) >= CGRectGetWidth(translatedFrame) * -1))
        {
            _masterHolderView.frame = translatedFrame;
        }
    }
    else if (gestureRecognizer.state == UIGestureRecognizerStateEnded || gestureRecognizer.state == UIGestureRecognizerStateCancelled)
    {
        if (_panDirectionRight && (_masterHolderView.frame.origin.x > (_detailHolderView.frame.origin.x - _masterHolderView.frame.size.width * 0.50)))
        {
            [self openSideDrawerWithAnimation:NO];
        }
        else if (!_panDirectionRight && (_masterHolderView.frame.origin.x < (_detailHolderView.frame.origin.x - _masterHolderView.frame.size.width * 0.50)))
        {
            [self closeSideDrawerWithAnimation:NO];
        }
        else
        {
            CGRect frame = _masterHolderView.frame;
            frame.origin.x = (_panDirectionRight) ? (_detailHolderView.frame.origin.x - _masterHolderView.frame.size.width) : _detailHolderView.frame.origin.x;
            [UIView animateWithDuration:SideDrawerAnimationSpeed animations:^{
                _masterHolderView.frame = frame;
            } completion:^(BOOL finished) {
                if (_panDirectionRight)
                {
                    [self.view sendSubviewToBack:_masterHolderView];
                }
            }];
        }
        _frameAtPanBegan = CGRectZero;
        _panDirectionRight = NO;
        _detailHolderView.userInteractionEnabled = YES;
    }
}

#pragma mark - MainScreenViewDelegate Methods

- (void)handleTopBarEvent:(TopBarViewEvent)event
{
    switch (event)
    {
        case TopBarViewEventShowMenu:
            [self openSideDrawerWithAnimation:YES];
            break;
            
        case TopBarViewEventHideMenu:
            [self closeSideDrawerWithAnimation:YES];
            break;
            
        case TopBarViewEventShowSearch:
            [self showSearch];
            break;
            
        case TopBarViewEventHideSearch:
            
            break;
        default:
            break;
    }
}

- (void)loadItemWithURL:(NSString *)aURLString
{
    _webViewVCURLString = aURLString;
    [self performSegueWithIdentifier:NSStringFromClass([WebViewVC class]) sender:self];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:NSStringFromClass([WebViewVC class])])
    {
        WebViewVC *webViewVC = (WebViewVC *)segue.destinationViewController;
        __unused CGRect dummyFrame = webViewVC.view.frame;
        [webViewVC loadURL:_webViewVCURLString];
    }
}

@end
