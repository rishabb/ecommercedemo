//
//  MainScreenView.h
//  eCommerceDemo
//
//  Created by Rishab Bokaria on 02/04/16.
//  Copyright © 2016 Rishab Bokaria. All rights reserved.
//

#import "CustomizableView.h"
#import "TopBarView.h"
#import "SideDrawerView.h"

@protocol MainScreenViewDelegate <TopBarViewDelegate>
- (void)loadItemWithURL:(NSString *)aURLString;
@end

@interface MainScreenView : CustomizableView <SideDrawerViewDelegate>

- (void)setDelegate:(__weak id<MainScreenViewDelegate>)delegate;
- (void)setMenuSelected:(BOOL)selected;

@end
