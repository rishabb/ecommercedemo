//
//  MainScreenView.m
//  eCommerceDemo
//
//  Created by Rishab Bokaria on 02/04/16.
//  Copyright © 2016 Rishab Bokaria. All rights reserved.
//

#import "MainScreenView.h"
#import "TemplateListView.h"
#import "UIView+Toast.h"

float const ScreenLoadAnimationDuration           =   0.3;
float const ScreenTransitionsAnimationDuration    =   0.5;

@interface MainScreenView ()<TemplateListViewDelegate>
{
    __weak IBOutlet TopBarView *_topBarView;
    __weak IBOutlet UIView *_holderView;
    
    __weak id<MainScreenViewDelegate> _delegate;
    __weak UIView *_currentScreenView;
    BOOL _isAnimationInProgress;
}
@end

@implementation MainScreenView


#pragma mark - View Initialization

- (void)nibLoaded
{
    [super nibLoaded];
}

- (void)setDelegate:(__weak id<MainScreenViewDelegate>)delegate
{
    _delegate = delegate;
    [_topBarView setDelegate:_delegate];
}

- (void)setMenuSelected:(BOOL)selected
{
    [_topBarView setMenuButtonSelected:selected];
}

#pragma mark - Helper Methods

- (void)loadHomeScreen
{
    TemplateListView *templateListView = [[TemplateListView alloc] init];
    [templateListView setDelegate:self];
    [templateListView setTemplates:[self getTemplatesFromLocalData]];
    [self loadViewOnHolderView:templateListView];
}

- (NSArray *)getTemplatesFromLocalData
{
    NSMutableArray *templates = [[NSMutableArray alloc] init];

    NSString *localPath = [[NSBundle mainBundle] pathForResource:@"f_one" ofType:@"json"];
    NSData *localData = [[NSData alloc] initWithContentsOfFile:localPath];
    __unused NSString *jsonString = [[NSString alloc] initWithData:localData encoding:NSUTF8StringEncoding];
    // NSLog(@"\n\n%@\n\n",jsonString);
  
    NSError *jsonError = nil;
    NSArray *templateInfoArray = [NSJSONSerialization JSONObjectWithData:localData options:NSJSONReadingMutableContainers error:&jsonError];
    if (!jsonError && templateInfoArray)
    {
        for (NSDictionary *templateInfo in templateInfoArray)
        {
            TemplateVO *template = [[TemplateVO alloc] initWithTemplateInfo:templateInfo];
            [templates addObject:template];
        }
    }
    
    return [[NSArray alloc] initWithArray:templates];
}

- (void)loadViewOnHolderView:(UIView *)nextView
{
    if (!_isAnimationInProgress)
    {
        _isAnimationInProgress = YES;
        [[UIApplication sharedApplication] beginIgnoringInteractionEvents];//block interaction as the animation has started.
        /*add the next view and remove the previous view.*/
        __block UIView *previousScreenView = _currentScreenView;
        _currentScreenView = nextView;
        
        //now add the next view.
        [_currentScreenView removeFromSuperview];
        [_holderView addSubview:_currentScreenView];
        
        [_currentScreenView setTranslatesAutoresizingMaskIntoConstraints:NO];
        
        NSLayoutConstraint *leading = [NSLayoutConstraint constraintWithItem:_holderView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:_currentScreenView attribute:NSLayoutAttributeLeading multiplier:1 constant:0];
        NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:_holderView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:_currentScreenView attribute:NSLayoutAttributeTop multiplier:1 constant:0];
        NSLayoutConstraint *trailing = [NSLayoutConstraint constraintWithItem:_holderView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:_currentScreenView attribute:NSLayoutAttributeTrailing multiplier:1 constant:0];
        NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem:_holderView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:_currentScreenView attribute:NSLayoutAttributeBottom multiplier:1 constant:0];
        [_holderView addConstraints:@[leading,top,bottom,trailing]];
        [_holderView setHidden:NO];
        
        //initially the next view is blurred.
        _currentScreenView.alpha = 0.25f;
        previousScreenView.alpha = 1.0f;
        
        //gradually the current view is blurred and next view is unblurred.
        [UIView animateWithDuration:ScreenLoadAnimationDuration delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            previousScreenView.alpha = 0.25f;
            _currentScreenView.alpha = 1.0f;
        } completion:^(BOOL finished) {
            [previousScreenView removeFromSuperview];
            [[UIApplication sharedApplication] endIgnoringInteractionEvents];//allow interaction as the animation has complted.
            _isAnimationInProgress = NO;
        }];
    }
    else
    {
        [self performSelector:@selector(loadViewOnHolderView:) withObject:nextView afterDelay:ScreenLoadAnimationDuration+0.1];
    }
}

#pragma mark - SideDrawerViewDelegate Methods

- (void)handleSideDrawerEvent:(SideDrawerViewEvent)event
{
    switch (event)
    {
        case SideDrawerViewEventHome:
            [self loadHomeScreen];
            break;
            
        default:
            break;
    }
}

#pragma mark - TemplateListViewDelegate Methods

- (void)didClickOnBuyButtonForItem:(ItemVO *)item
{
    [self makeToast:@"Item added to cart."];
}

- (void)didClickOnItem:(ItemVO *)item
{
    if (_delegate && [_delegate respondsToSelector:@selector(loadItemWithURL:)])
    {
        [_delegate loadItemWithURL:[item webURL]];
    }
}



@end
