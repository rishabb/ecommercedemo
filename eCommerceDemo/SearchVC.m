//
//  SearchVC.m
//  eCommerceDemo
//
//  Created by Rishab Bokaria on 04/04/16.
//  Copyright © 2016 Rishab Bokaria. All rights reserved.
//

#import "SearchVC.h"

@interface SearchVC ()

@end

@implementation SearchVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)didClickOnBackButton:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
