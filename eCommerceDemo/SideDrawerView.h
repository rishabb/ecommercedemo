//
//  SideDrawerView.h
//  eCommerceDemo
//
//  Created by Rishab Bokaria on 02/04/16.
//  Copyright © 2016 Rishab Bokaria. All rights reserved.
//

#import "CustomizableView.h"

typedef enum
{
    SideDrawerViewEventNone,
    SideDrawerViewEventHome
} SideDrawerViewEvent;

@protocol SideDrawerViewDelegate <NSObject>

- (void)handleSideDrawerEvent:(SideDrawerViewEvent)event;

@end

@interface SideDrawerView : CustomizableView

- (void)setDelegate:(__weak id<SideDrawerViewDelegate>)delegate;

@end
