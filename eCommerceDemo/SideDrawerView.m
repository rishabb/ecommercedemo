//
//  SideDrawerView.m
//  eCommerceDemo
//
//  Created by Rishab Bokaria on 02/04/16.
//  Copyright © 2016 Rishab Bokaria. All rights reserved.
//

#import "SideDrawerView.h"

NSString* const SIDE_DRAWER_VIEW_CELL_IDENTIFIER    =   @"SideDrawerViewCellIdentifer";

@interface SideDrawerView ()<UITableViewDataSource, UITableViewDelegate>
{
    __weak IBOutlet UITableView *_tableView;
    __weak id<SideDrawerViewDelegate> _delegate;
    
    NSDictionary *_sideMenuOptionInfo;
    SideDrawerViewEvent _currentEventOption;
}
@end

@implementation SideDrawerView

#pragma mark - View Initialization

- (void)nibLoaded
{
    [super nibLoaded];
    [self setUpMenuOptions];
    [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:SIDE_DRAWER_VIEW_CELL_IDENTIFIER];
}

- (void)setDelegate:(__weak id<SideDrawerViewDelegate>)delegate
{
    _delegate = delegate;
}

#pragma mark - Helper Methods

- (void)setUpMenuOptions
{
    _sideMenuOptionInfo = @{@"Home":@(SideDrawerViewEventHome)};
    [_tableView reloadData];
}

- (void)callDelegateForEventType:(SideDrawerViewEvent)eventType
{
    if (_currentEventOption != eventType)
    {
        _currentEventOption = eventType;
        if (_delegate && [_delegate respondsToSelector:@selector(handleSideDrawerEvent:)])
        {
            [_delegate handleSideDrawerEvent:eventType];
        }
    }
}

#pragma mark - UITableViewDataSource & UITableViewDelegate Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _sideMenuOptionInfo.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:SIDE_DRAWER_VIEW_CELL_IDENTIFIER];
    NSString *key = [[_sideMenuOptionInfo allKeys] objectAtIndex:indexPath.row];
    cell.textLabel.text = NSLocalizedString(key, nil);
    cell.contentView.backgroundColor = cell.backgroundView.backgroundColor = cell.backgroundColor = [UIColor clearColor];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18.0f];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *key = [[_sideMenuOptionInfo allKeys] objectAtIndex:indexPath.row];
    SideDrawerViewEvent event = (SideDrawerViewEvent)[[_sideMenuOptionInfo valueForKey:key] integerValue];
    if (event != SideDrawerViewEventNone)
    {
        [self callDelegateForEventType:event];
    }
}


@end
