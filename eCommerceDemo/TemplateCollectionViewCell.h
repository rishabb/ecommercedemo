//
//  TemplateCollectionViewCell.h
//  eCommerceDemo
//
//  Created by Rishab Bokaria on 03/04/16.
//  Copyright © 2016 Rishab Bokaria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ItemVO.h"

@protocol TemplateCollectionViewCellDelegate <NSObject>

- (void)didClickOnBuyButtonForItem:(ItemVO *)item;

@end

@interface TemplateCollectionViewCell : UICollectionViewCell

- (void)setDelegate:(__weak id<TemplateCollectionViewCellDelegate>)delegate;
- (void)setItem:(ItemVO *)item;
- (void)setItemInfoHidden:(BOOL)hidden;

@end
