//
//  TemplateCollectionViewCell.m
//  eCommerceDemo
//
//  Created by Rishab Bokaria on 03/04/16.
//  Copyright © 2016 Rishab Bokaria. All rights reserved.
//

#import "TemplateCollectionViewCell.h"
#import "UIImageView+AFNetworking.h"

@interface TemplateCollectionViewCell ()
{
    __weak IBOutlet UIImageView *_imageView;
    __weak IBOutlet UILabel *_label;
    __weak IBOutlet UIButton *_buyButton;
    __weak IBOutlet UILabel *_priceLabel;
    
    __weak id<TemplateCollectionViewCellDelegate> _delegate;
    ItemVO *_item;
}
- (IBAction)didClickOnBuyButton:(UIButton *)sender;
@end

@implementation TemplateCollectionViewCell

#pragma mark - View Initialization

- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    _item = nil;
    _label.text = @"";
    _imageView.image = [UIImage imageNamed:@"eCommerce.jpg"];
    [self setItemInfoHidden:NO];
}

- (void)setDelegate:(__weak id<TemplateCollectionViewCellDelegate>)delegate
{
    _delegate = delegate;
}

- (void)setItem:(ItemVO *)item;
{
    _item = item;
    [self updateItemInfo];
}

#pragma mark - Helper Methods

- (void)updateItemInfo
{
    if (_item)
    {
        _label.text = [_item label];
        [_imageView setImageWithURL:[NSURL URLWithString:[_item imageURL]] placeholderImage:[UIImage imageNamed:@"eCommerce.jpg"]];
    }
}

- (void)setItemInfoHidden:(BOOL)hidden
{
    _label.hidden = _priceLabel.hidden = _buyButton.hidden = hidden;
}

#pragma mark - Action Methods

- (IBAction)didClickOnBuyButton:(UIButton *)sender
{
    if (_delegate && [_delegate respondsToSelector:@selector(didClickOnBuyButtonForItem:)])
    {
        [_delegate didClickOnBuyButtonForItem:_item];
    }
}

@end
