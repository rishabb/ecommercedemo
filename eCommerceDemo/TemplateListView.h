//
//  TemplateListView.h
//  eCommerceDemo
//
//  Created by Rishab Bokaria on 03/04/16.
//  Copyright © 2016 Rishab Bokaria. All rights reserved.
//

#import "CustomizableView.h"
#import "TemplateListViewCell.h"

@protocol TemplateListViewDelegate <TemplateListViewCellDelegate>

@end

@interface TemplateListView : CustomizableView

- (void)setDelegate:(__weak id<TemplateListViewDelegate>)delegate;
- (void)setTemplates:(NSArray<TemplateVO *> *)templates;

@end
