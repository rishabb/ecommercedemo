//
//  TemplateListView.m
//  eCommerceDemo
//
//  Created by Rishab Bokaria on 03/04/16.
//  Copyright © 2016 Rishab Bokaria. All rights reserved.
//

#import "TemplateListView.h"
@interface TemplateListView ()<UITableViewDataSource, UITableViewDelegate>
{
    __weak IBOutlet UITableView *_tableView;
    __weak id<TemplateListViewDelegate> _delegate;
    NSArray <TemplateVO *> *_templates;
}
@end

@implementation TemplateListView

#pragma mark - View Initialization

- (void)nibLoaded
{
    [super nibLoaded];
    [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([TemplateListViewCell class]) bundle:[NSBundle mainBundle]] forCellReuseIdentifier:NSStringFromClass([TemplateListViewCell class])];
}

- (void)setDelegate:(__weak id<TemplateListViewDelegate>)delegate
{
    _delegate = delegate;
}

- (void)setTemplates:(NSArray<TemplateVO *> *)templates
{
    _templates = templates;
    [_tableView reloadData];
}

#pragma mark - UITableViewDataSource, UITableViewDelegate Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _templates.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TemplateListViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([TemplateListViewCell class])];
    [cell setDelegate:_delegate];
    [cell setTemplate:_templates[indexPath.row]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TemplateVO *template = _templates[indexPath.row];
    return ([template type] == TemplateType2)?240.0f:190.0f;
}

@end
