//
//  TemplateListViewCell.h
//  eCommerceDemo
//
//  Created by Rishab Bokaria on 03/04/16.
//  Copyright © 2016 Rishab Bokaria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TemplateVO.h"
#import "TemplateCollectionViewCell.h"

@protocol TemplateListViewCellDelegate <TemplateCollectionViewCellDelegate>

- (void)didClickOnItem:(ItemVO *)item;

@end

@interface TemplateListViewCell : UITableViewCell

- (void)setDelegate:(__weak id<TemplateListViewCellDelegate>)delegate;
- (void)setTemplate:(TemplateVO *)template;

@end
