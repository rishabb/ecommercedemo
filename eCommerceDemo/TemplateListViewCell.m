//
//  TemplateListViewCell.m
//  eCommerceDemo
//
//  Created by Rishab Bokaria on 03/04/16.
//  Copyright © 2016 Rishab Bokaria. All rights reserved.
//

#import "TemplateListViewCell.h"

@interface TemplateListViewCell ()<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
{
    __weak IBOutlet UICollectionView *_collectionView;
    __weak IBOutlet UILabel *_label;
    __weak IBOutlet NSLayoutConstraint *_labelHeightConstraint;
    __weak IBOutlet UIPageControl *_pageControl;
    __weak id<TemplateListViewCellDelegate> _delegate;
    TemplateVO *_template;
}
- (IBAction)didChangePage:(UIPageControl *)sender;
@end

@implementation TemplateListViewCell

#pragma mark - View Initialization

- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code
    [_collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([TemplateCollectionViewCell class]) bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:NSStringFromClass([TemplateCollectionViewCell class])];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    _label.text = @"";
    _labelHeightConstraint.constant = 24.0f;
    [self setNeedsLayout];
    _template = nil;
    _pageControl.numberOfPages = 1;
    _pageControl.currentPage = 0;
    _pageControl.hidden = YES;
    [_collectionView reloadData];
    _collectionView.bounces = YES;
    _collectionView.pagingEnabled = NO;
}

- (void)setDelegate:(__weak id<TemplateListViewCellDelegate>)delegate
{
    _delegate = delegate;
}

- (void)setTemplate:(TemplateVO *)template
{
    _template = template;
    [self updateTemplateInfo];
}

#pragma mark - Helper Methods

- (void)updateTemplateInfo
{
    if (_template)
    {
        _label.text = [_template label];
        [self updateLabelHeight];
        [self updatePageControl];
        [self updateCollectionView];
    }
}

- (void)updateLabelHeight
{
    if ([_template type] == TemplateType1)
    {
        _labelHeightConstraint.constant = 0.0f;
        [self setNeedsLayout];
    }
}

- (void)updatePageControl
{
    _pageControl.numberOfPages = _template.items.count;
    _pageControl.currentPage = 0;
    _pageControl.hidden = (([_template type] == TemplateType2) || _pageControl.numberOfPages < 2);
}

- (void)updateCollectionView
{
    if ([_template type] != TemplateType2)
    {
        _collectionView.pagingEnabled = YES;
        _collectionView.bounces = NO;
    }
    [_collectionView reloadData];
}

#pragma mark - UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout Methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _template.items.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TemplateCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([TemplateCollectionViewCell class]) forIndexPath:indexPath];
    [cell setDelegate:_delegate];
    [cell setItemInfoHidden:([_template type] != TemplateType2)];
    [cell setItem:_template.items[indexPath.item]];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    ItemVO *item = _template.items[indexPath.item];
    if (_delegate && [_delegate respondsToSelector:@selector(didClickOnItem:)])
    {
        [_delegate didClickOnItem:item];
    }
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return ([_template type] == TemplateType2)?CGSizeMake(150.0f, 200.0f):CGSizeMake(collectionView.bounds.size.width, 150.0f);
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGFloat pageWidth = _collectionView.frame.size.width;
    float currentPage = _collectionView.contentOffset.x / pageWidth;
    
    if (0.0f != fmodf(currentPage, 1.0f))
    {
        _pageControl.currentPage = currentPage + 1;
    }
    else
    {
        _pageControl.currentPage = currentPage;
    }
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:_pageControl.currentPage inSection:0];
    [_collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
}

#pragma mark - Action Methods

- (IBAction)didChangePage:(UIPageControl *)sender
{
    @try
    {
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:_pageControl.currentPage inSection:0];
        [_collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    }
    @catch (NSException *exception)
    {
        _pageControl.currentPage = 0;
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:_pageControl.currentPage inSection:0];
        [_collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    }
}

@end
