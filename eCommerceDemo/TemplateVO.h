//
//  TemplateVO.h
//  eCommerceDemo
//
//  Created by Rishab Bokaria on 03/04/16.
//  Copyright © 2016 Rishab Bokaria. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ItemVO.h"

typedef enum
{
    TemplateType1,
    TemplateType2,
    TemplateType3,
    TemplateType4
} TemplateType;

@interface TemplateVO : NSObject

- (instancetype)initWithTemplateInfo:(NSDictionary *)templateInfo;

- (NSString *)label;
- (NSString *)imageURL;
- (TemplateType)type;
- (NSArray *)items;

@end
