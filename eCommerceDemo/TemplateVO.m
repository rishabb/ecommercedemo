//
//  TemplateVO.m
//  eCommerceDemo
//
//  Created by Rishab Bokaria on 03/04/16.
//  Copyright © 2016 Rishab Bokaria. All rights reserved.
//

#import "TemplateVO.h"
#import "AppUtils.h"

NSString* const TEMPLATE_INFO_LABEL_KEY     =   @"label";
NSString* const TEMPLATE_INFO_IMAGE_KEY     =   @"image";
NSString* const TEMPLATE_INFO_TEMPLATE_KEY  =   @"template";
NSString* const TEMPLATE_INFO_ITEMS_KEY     =   @"items";

NSString* const TEMPLATE_INFO_TEMPLATE_TYPE1    =   @"product-template-1";
NSString* const TEMPLATE_INFO_TEMPLATE_TYPE2    =   @"product-template-2";
NSString* const TEMPLATE_INFO_TEMPLATE_TYPE3    =   @"product-template-3";

@interface TemplateVO ()
{
    NSString *_labelString;
    NSString *_imageURL;
    TemplateType _type;
    NSArray *_items;
}
@end

@implementation TemplateVO

- (instancetype)initWithTemplateInfo:(NSDictionary *)templateInfo
{
    self = [super init];
    if (self)
    {
        _labelString = [AppUtils clearNull:[templateInfo valueForKey:TEMPLATE_INFO_LABEL_KEY]];
        _imageURL = [AppUtils clearNull:[templateInfo valueForKey:TEMPLATE_INFO_IMAGE_KEY]];
        _type = [self getTemplateTypeFromTemplateString:[AppUtils clearNull:[templateInfo valueForKey:TEMPLATE_INFO_TEMPLATE_KEY]]];
        _items = [self getItemsFromItemInfos:[templateInfo valueForKey:TEMPLATE_INFO_ITEMS_KEY]];
    }
    return self;
}

- (NSString *)label
{
    return _labelString;
}

- (NSString *)imageURL
{
    return _imageURL;
}

- (TemplateType)type
{
    return _type;
}

- (NSArray *)items
{
    return _items;
}

- (TemplateType)getTemplateTypeFromTemplateString:(NSString *)template
{
    TemplateType type = TemplateType1;
    if ([template isEqualToString:TEMPLATE_INFO_TEMPLATE_TYPE1])
    {
        type = TemplateType1;
    }
    else if ([template isEqualToString:TEMPLATE_INFO_TEMPLATE_TYPE2])
    {
        type = TemplateType2;
    }
    else if ([template isEqualToString:TEMPLATE_INFO_TEMPLATE_TYPE3])
    {
        type = TemplateType3;
    }
    return type;
}

- (NSArray *)getItemsFromItemInfos:(NSArray *)itemInfos
{
    NSMutableArray *items = [[NSMutableArray alloc] init];
    for (NSDictionary *itemInfo in itemInfos)
    {
        ItemVO *item = [[ItemVO alloc] initWithItemInfo:itemInfo];
        [items addObject:item];
    }
    return [[NSArray alloc] initWithArray:items];
}

@end
