//
//  TopBarView.h
//  eCommerceDemo
//
//  Created by Rishab Bokaria on 02/04/16.
//  Copyright © 2016 Rishab Bokaria. All rights reserved.
//

#import "CustomizableView.h"

typedef enum
{
    TopBarViewEventNone,
    TopBarViewEventShowMenu,
    TopBarViewEventHideMenu,
    TopBarViewEventShowSearch,
    TopBarViewEventHideSearch,
    TopBarViewEventShowLocations,
    TopBarViewEventHideLocations
} TopBarViewEvent;


@protocol TopBarViewDelegate <NSObject>

- (void)handleTopBarEvent:(TopBarViewEvent)event;

@end

@interface TopBarView : CustomizableView

- (void)setDelegate:(__weak id<TopBarViewDelegate>)delegate;
- (void)setMenuButtonSelected:(BOOL)selected;

@end
