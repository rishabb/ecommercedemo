//
//  TopBarView.m
//  eCommerceDemo
//
//  Created by Rishab Bokaria on 02/04/16.
//  Copyright © 2016 Rishab Bokaria. All rights reserved.
//

#import "TopBarView.h"

@interface TopBarView ()
{
    __weak IBOutlet UIButton *_menuButton;
    __weak IBOutlet UIButton *_searchButton;
    
    __weak id<TopBarViewDelegate> _delegate;
    
    TopBarViewEvent _currentEventOption;
}

@end

@implementation TopBarView

#pragma mark - View Initialization

- (void)nibLoaded
{
    [super nibLoaded];
}

- (void)setDelegate:(__weak id<TopBarViewDelegate>)delegate
{
    _delegate = delegate;
}

- (void)setMenuButtonSelected:(BOOL)selected
{
    _menuButton.selected = selected;
}

#pragma mark - Helper Methods

- (void)callDelegateForEventType:(TopBarViewEvent)eventType
{
    if (_currentEventOption != eventType)
    {
        _currentEventOption = eventType;
        if (_delegate && [_delegate respondsToSelector:@selector(handleTopBarEvent:)])
        {
            [_delegate handleTopBarEvent:eventType];
        }
    }
}

#pragma mark - Action Methods

- (IBAction)didClickOnMenuButton:(UIButton *)sender
{
    [self callDelegateForEventType:sender.selected?TopBarViewEventHideMenu:TopBarViewEventShowMenu];
    sender.selected = !sender.selected;
}

- (IBAction)didClickOnSearchButton:(UIButton *)sender
{
    _currentEventOption = TopBarViewEventNone;
    [self callDelegateForEventType:TopBarViewEventShowSearch];
    sender.selected = !sender.selected;
}

@end
