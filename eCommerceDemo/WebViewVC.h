//
//  WebViewVC.h
//  eCommerceDemo
//
//  Created by Rishab Bokaria on 04/04/16.
//  Copyright © 2016 Rishab Bokaria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewVC : UIViewController

- (void)loadURL:(NSString *)aURLString;

@end
