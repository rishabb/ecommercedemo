//
//  WebViewVC.m
//  eCommerceDemo
//
//  Created by Rishab Bokaria on 04/04/16.
//  Copyright © 2016 Rishab Bokaria. All rights reserved.
//

#import "WebViewVC.h"

@interface WebViewVC ()
{
    __weak IBOutlet UIWebView *_webView;
    
}
- (IBAction)didClickOnBackButton:(UIButton *)sender;
@end

@implementation WebViewVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadURL:(NSString *)aURLString
{
    NSURL *aURL = [NSURL URLWithString:aURLString];
    NSURLRequest *aURLRequest = [NSURLRequest requestWithURL:aURL];
    [_webView loadRequest:aURLRequest];
}

- (IBAction)didClickOnBackButton:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
